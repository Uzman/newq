package com.consultation.system;

import com.consultation.problem.*;

import com.example.q.R;

import android.app.Activity;
import android.os.Bundle;

public class MainActivity extends Activity {
	
	ProblemsFragment problemsFragment;
	
	com.consultation.suggestion.SuggestionsFragment suggestionsFragment;
	
	com.consultation.solution.SolutionsFragment solutionsFragment;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        getFragmentManager().beginTransaction().add(R.id.content_frame, new ProblemsFragment()).commit();
    }
}

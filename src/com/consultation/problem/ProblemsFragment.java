package com.consultation.problem;

import java.util.ArrayList;
import java.util.List;

import com.consultation.system.MainActivity;
import com.example.q.R;

import android.os.Bundle;
import android.app.Fragment;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ListView;

public class ProblemsFragment extends Fragment implements OnItemClickListener{
	
	ListView problemsListView;
	ProblemsListAdapter problemsListAdapter;
	List<Problem> problemList = new ArrayList<Problem>();
	List<Problem> filteredProblems = new ArrayList<Problem>();
	ListView q ;
	@Override
	public void onCreate(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onCreate(savedInstanceState);

		problemList.add(new Problem(1,"asd"));
		problemList.add(new Problem(2,"möç"));
		problemList.add(new Problem(3,"vbn"));
		problemList.add(new Problem(4,"zxc"));
		problemList.add(new Problem(5,"şi,"));
		problemList.add(new Problem(6,"rgnhg"));
		problemList.add(new Problem(7,"jsfkvb"));
		problemList.add(new Problem(8,"asddfgasf"));
		problemList.add(new Problem(9,"uıo"));
		problemList.add(new Problem(10,"dsgsasfdb"));
		problemList.add(new Problem(11,"kjfn"));
		problemList.add(new Problem(12,"asd"));
		problemList.add(new Problem(13,"dfgh"));
		problemList.add(new Problem(14,"verf"));
		problemList.add(new Problem(15,"zxc"));
		problemList.add(new Problem(16,"şi,"));
		problemList.add(new Problem(17,"gjhv"));
		problemList.add(new Problem(18,"dsvnf"));
		problemList.add(new Problem(19,"drhx"));
		problemList.add(new Problem(20,"cgfhc"));
		problemList.add(new Problem(21,"nokbhjk"));
		problemList.add(new Problem(22,"ompkö"));
		problemList.add(new Problem(23,"klmn"));
		problemList.add(new Problem(24,"tfcjk"));
		problemList.add(new Problem(25,"çğl.ü"));
		problemList.add(new Problem(26,"çişçlğç"));
		problemList.add(new Problem(27,"çlpkö,"));
		problemList.add(new Problem(28,"çğlpkoö"));
		problemList.add(new Problem(29,"klşopjım"));
		problemList.add(new Problem(30,"pjokıhmn"));
		problemList.add(new Problem(31,"humn"));
		problemList.add(new Problem(32,"vhjı"));
		problemList.add(new Problem(33,"pömbhokm"));
		problemList.add(new Problem(34,"ogıpş"));
		problemList.add(new Problem(35,"yfcrb"));
		problemList.add(new Problem(36,"dfgjkğ"));
		problemList.add(new Problem(37,"öçoöık"));
		problemList.add(new Problem(38,"hgfcvy,"));
		problemList.add(new Problem(39,"fgh"));
		problemList.add(new Problem(40,"vyhk"));
		problemList.add(new Problem(41,"vıygvf"));
		problemList.add(new Problem(42,"jkhbnlph"));
		problemList.add(new Problem(43,"gıyrcxu"));
		problemList.add(new Problem(44,"möçhıj"));
		problemsListAdapter = new ProblemsListAdapter(this,(MainActivity)this.getActivity(), problemList);
	}

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		try{
		super.onCreateView(inflater, container, savedInstanceState);
		View v = inflater.inflate(R.layout.problems_fragment, container,false);
		q = (ListView)v.findViewById(R.id.listView1);
		Button sendButton = (Button)v.findViewById(R.id.txt_bar);
		sendButton.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				
			}
		});
		final EditText edt = (EditText)v.findViewById(R.id.problem_txt_field);
		
		edt.addTextChangedListener(new TextWatcher() {
			
			@Override
			public void onTextChanged(CharSequence arg0, int arg1, int arg2, int arg3) {

			}
			
			@Override
			public void beforeTextChanged(CharSequence arg0, int arg1, int arg2,
					int arg3) {
				
			}
			
			@Override
			public void afterTextChanged(Editable arg0) {
				refilterRecords(edt.getText().toString());
			}
		});
		q.setOnItemClickListener(this);
		q.setAdapter(problemsListAdapter);

		return v;
		} catch(Exception ex){
			ex.printStackTrace();
			int s = 5; 
			s*=s;
		}
		return null;
	}
	
	public static void problemChoosen(int id){
		
	}
	

	private void refilterRecords(String txt){
		txt= ".*"+txt+".*";
		String[] keyWords = txt.split(" ");
		filteredProblems.clear();
		for(int i = 0 ; i <problemList.size(); i++){
			Problem p = problemList.get(i);
			if(p.problemString.matches(txt)){
				filteredProblems.add(p);
			}
		}
		problemsListAdapter.problems = filteredProblems;
		problemsListAdapter.notifyDataSetChanged();
	}


	@Override
	public void onItemClick(AdapterView<?> arg0, View arg1, int arg2, long arg3) {
		// TODO Auto-generated method stub
		
	}
	
	public void goToSolutions(int problemID){
		
	}
}

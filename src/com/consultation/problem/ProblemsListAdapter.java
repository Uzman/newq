package com.consultation.problem;

import java.util.List;

import com.consultation.system.MainActivity;
import com.example.q.R;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

public class ProblemsListAdapter extends BaseAdapter {
	Context context;
	
	
	List<Problem> problems ;
	
	ProblemsFragment fragment;
	
			
	public void updateValues(){
		
	}
	
	public ProblemsListAdapter(ProblemsFragment fragmen,MainActivity mainInstance,List<Problem> problems){
		super();
		this.fragment = fragmen;
		this.context = mainInstance;
		this.problems = problems;
	}

	@Override
	public int getCount() {
		// TODO Auto-generated method stub
		return problems.size();
	}

	@Override
	public Object getItem(int arg0) {
		// TODO Auto-generated method stub
		return null;
	}

	
	public class ViewHolder{
		TextView text;
		ImageView imageView;
		Problem problem;
		public ViewHolder(TextView textView,ImageView imageView){
			this.text = textView;
			this.imageView = imageView;
		}
	}
	
	@Override
	public View getView(final int arg0, View arg1, ViewGroup arg2) {

		ViewHolder holder;
		if(arg1 == null){
			LayoutInflater mInflater = LayoutInflater.from(this.getContext());
			arg1= mInflater.inflate(R.layout.problem_row, arg2,false);
			View t =arg1.findViewById(R.id.entryText);
			ImageView imgBtn = (ImageView) arg1.findViewById(R.id.btn_solution);
			
			holder = new ViewHolder((TextView)t,imgBtn);
			arg1.setTag(holder);
		} else{
			holder = (ViewHolder) arg1.getTag();
		}
		holder.text.setText(arg0+"	"+problems.get(arg0).problemString);
		holder.imageView.setOnClickListener(new View.OnClickListener() {
			
			@Override
			public void onClick(View view) {
				int m = arg0;
				fragment.goToSolutions(problems.get(m).problemID);
			}
		});
		return arg1;
	}
	
	private Context getContext(){
		return context;
	}

	@Override
	public long getItemId(int arg0) {
		// TODO Auto-generated method stub
		return arg0;
	}

}
